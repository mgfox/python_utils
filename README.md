# python_utils
Small python utils/helpers

### api/telegram_api.py

simple Telegram API 

### procesing/jsonpath.py

simple implementation of json path using parsed JSON document

### processing/trigram.py

`TrigramSet` is the way to search similar words with trigram algorithm

### processing/scheduler.py

Simple decorator for job scheduled execution

Examples:

#### every 30 minutes working days

    @scheduled(every="30", days=['MON', 'TUE', 'WED', 'THU', 'FRI'])
    def test():
        print datetime.datetime.now().isoformat()

#### at noon daily

    @scheduled(at="12:00")
    def test():
        print "Noon"

#### every Wednesday at midnight

    @scheduled(days=['WED'])
    def test():
        print "Wednesday midnight"

### testing/perforzavr.py

Simple (primitive) standalone tool for performance tesing of web services

### web/logging_helper.py

Helper for logging web requests and methods 
* `init_app_logging` - installs hooks for [bottlepy](http://bottlepy.org) application to log all requests with timing
* `logged` - decorator logging function calls with parameters and results

### web/loginza.py

API for integrate [bottlepy](http://bottlepy.org) application with [loginza](https://loginza.ru/) auth provider

### wiki/creole2html.py

html generator from [wiki creole](https://en.wikipedia.org/wiki/Creole_(markup))
