#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json
import time
import urllib2
import threading
import logging


class PoolThread(threading.Thread):
    def __init__(self, interval, function):
        threading.Thread.__init__(self, name="pool-thread")
        self.daemon = True
        self.interval = interval
        self.active = True
        self.function = function

    def run(self):
        while self.active:
            time.sleep(self.interval)
            self.function()


class TelegramAPI(object):
    BOT_API = "https://api.telegram.org/bot%(token)s/%(method)s"
    HEADERS = {"Content-Type": "application/json; charset=utf-8"}

    class METHOD(object):
        SET_WEBHOOK = "setWebhook"
        GET_UPDATES = "getUpdates"
        SEND_MESSAGE = "sendMessage"
        DELETE_WEBHOOK = "deleteWebhook"

    class TAG(object):
        RESULT = "result"
        UPDATE_ID = "update_id"
        CHAT_ID = "chat_id"
        REPLY_TO = "reply_to_message_id"

    def __init__(self, token):
        self.logger = logging.getLogger("telegram_api")
        self.token = token
        self.max_update_id = -100
        self.updates = {}

    def __exec__(self, method, params=None):
        self.logger.debug("call api: method=%s, params=%s", method, params)
        url = self.BOT_API % dict(token=self.token, method=method)
        if params:
            data = json.dumps(params)
        else:
            data = None
        request = urllib2.Request(url, data, headers=self.HEADERS)
        if data:
            request.get_method = lambda: "POST"
        return urllib2.urlopen(request)

    def set_webhook(self, url):
        return self.__exec__(self.METHOD.SET_WEBHOOK, dict(url=url))

    def delete_webhook(self):
        return self.__exec__(self.METHOD.DELETE_WEBHOOK)

    def get_updates(self, offset=None):
        if not offset:
            offset = self.max_update_id + 1
        params = dict(offset=offset, limit=100)
        updates_raw = self.__exec__(self.METHOD.GET_UPDATES, params).read()
        self.logger.debug("update response: raw=%s", updates_raw)
        updates = json.loads(updates_raw)
        for update in updates[self.TAG.RESULT]:
            if update.get(self.TAG.UPDATE_ID) > self.max_update_id:
                self.max_update_id = update.get(self.TAG.UPDATE_ID)
        return updates

    def pool_updates(self, interval, callback):
        self.logger.info("start pooling: interval=%s" % interval)

        def pool_func():
            try:
                updates = self.get_updates()
                for update in updates[self.TAG.RESULT]:
                    self.logger.debug("process update: %s", update)
                    callback(update)
            except Exception, e:
                self.logger.debug("update error: %s", e)
        pool_func()
        self.pool_thread = PoolThread(interval, pool_func)
        self.pool_thread.start()

    def stop_pooling(self):
        if self.pool_thread:
            self.pool_thread.active = False

    def send_message(self, chat_id, text, reply_to_message_id=None):
        params = dict(text=text)
        try:
            params[self.TAG.CHAT_ID] = int(chat_id)
        except ValueError:
            params[self.TAG.CHAT_ID] = chat_id
        if reply_to_message_id:
            params[self.TAG.REPLY_TO] = reply_to_message_id
        m = self.__exec__(self.METHOD.SEND_MESSAGE, params).read()
        return json.loads(m)
