# -*- coding: utf-8 -*-


def jsonpath(document, path):
    if type(document) == dict or type(document) == list:
        parts = path.split("/", 1)
        obj = document.get(parts[0])
        if len(parts) == 1:
            return obj
        else:
            if obj:
                return jsonpath(obj, parts[1])
    return None
