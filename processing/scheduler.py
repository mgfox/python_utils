#!/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'Mike VV'

import time
import datetime
import threading
import logging

DAYS = ["MON", "TUE", "WED", "THU", "FRI", "SAT", "SUN"]


class CronJob(object):
    def __init__(self, func, every="", at="", days=[]):
        self.tx = datetime.datetime.now()
        self.every = self.__parse_period__(every)
        self.at = self.__parse_time__(at)
        self.days = self.__parse_days__(days)
        self.func = func
        self.func_name = func.__name__
        self.executing = False

    def check_and_trigger(self, dt):
        if self.__check_days__(dt):
            if self.every:
                delta = dt.minute % self.every
                if delta == 0:
                    self.__trigger__(dt)
            elif self.at:
                if dt.hour == self.at[0] and dt.minute == self.at[1]:
                    self.__trigger__(dt)
            elif self.days:
                if dt.hour == 0 and dt.minute == 0:
                    self.__trigger__(dt)

    def __parse_period__(self, period):
        if period:
            return int(period)
        return None

    def __parse_time__(self, stime):
        if stime:
            parts = stime.split(":")
            return [int(parts[0]), int(parts[1])]
        return None

    def __parse_days__(self, days):
        if days:
            res = []
            for day in days:
                if day in DAYS:
                    res.append(days.index(day))
                else:
                    res.append(int(day))
            return res
        return None

    def __check_days__(self, dt):
        if self.days:
            if dt.weekday() not in self.days:
                return False
        return True

    def __trigger__(self, dt):
        if self.executing:
            logging.warn("function %s is executing" % self.func_name)
            return
        curr_time = dt.isoformat()
        logging.info("triggered %s() at %s" % (self.func_name, curr_time))
        th = threading.Thread(name="worker %s at %s" % (self.func_name,
                                                        curr_time))

        def threaded_func():
            self.executing = True
            self.func()
            self.executing = False
        th.run = threaded_func
        th.start()

    def __str__(self):
        if self.days:
            day_list = []
            for day in self.days:
                day_list.append(DAYS[day])
            days = " by (%s)" % ", ".join(day_list)
        else:
            days = ""
        if self.every:
            return "func '%s' every %dm%s" % (self.func_name,
                                              self.every,
                                              days)
        elif self.at:
            return "func '%s' at %d:%d%s" % (self.func_name,
                                             self.at[0],
                                             self.at[1],
                                             days)
        elif self.days:
            return "func '%s'%s" % (self.func_name, days)
        return "func '%s' without schedule" % self.func_name


class CronThread(threading.Thread):
    def __init__(self):
        name = "cron_thread"
        threading.Thread.__init__(self, name=name)
        self.log = logging.getLogger(name)
        self.job_list = []
        self.active = True
        self.setDaemon(True)
        self.start()

    def add_job(self, func, every="", at="", days=""):
        job = CronJob(func, every, at, days)
        self.job_list.append(job)
        self.log.info("added job %s" % job)

    def __sleep__(self):
        now = datetime.datetime.now()
        align = now.second + now.microsecond / 1000000.0
        sec_align = 60 - align
        if self.log.isEnabledFor(logging.DEBUG):
            self.log.debug("sleep %s" % sec_align)
        time.sleep(sec_align)

    def run(self):
        self.__sleep__()
        while self.active:
            now = datetime.datetime.now()
            if self.log.isEnabledFor(logging.DEBUG):
                self.log.debug("check %d jobs at %s" % (len(self.job_list),
                                                        now.isoformat()))
            for job in self.job_list:
                job.check_and_trigger(now)
            self.__sleep__()


__default_cron_thread__ = None


def scheduled(every="", at="", days=[]):
    """
    trigger decorated function by time/period

    :every - trigger function periodicaly (period in minutes)
    :at - trigger function at specific time (HH:MM)
    :days - trigger function at specific week days
            (MON, TUE, WED, THU, FRI, SAT, SUN)

    Examples:
    # every 30 minutes wokring days
    @scheduled(every="30", days=["MON", "TUE", "WED", "THU", "FRI"])
    def test():
        print datetime.datetime.now().isoformat()

    # at noon daily
    @scheduled(at="12:00")
    def test():
        print "Noon"

    # every Wednesday at midnight
    @scheduled(days=['WED'])
    def test():
        print "Wednesday midnight"
    """
    def scheduler_func(func):
        global __default_cron_thread__
        if not __default_cron_thread__:
            __default_cron_thread__ = CronThread()
        __default_cron_thread__.add_job(func, every, at, days)
        return func
    return scheduler_func
