# coding=utf-8
__author__ = 'Mike VV'

import unittest
import json
from processing.jsonpath import jsonpath

TEST_DOC = '''{"version": [1, 5, 17],
    "params": {"param2": {"a": 1, "c": 3, "b": 2}, "param1": "test param"},
    "method": "test", "request_id": 673245238}'''


class TestJsonPath(unittest.TestCase):
    def test_jsonpath(self):
        doc = json.loads(TEST_DOC)
        self.assertEqual(jsonpath(doc, "method"), "test")
        self.assertEqual(jsonpath(doc, "version"), [1,5,17])
        self.assertEqual(jsonpath(doc, "params/param2/c"), 3)
