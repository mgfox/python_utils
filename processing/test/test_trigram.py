#!/usr/bin/env python
# -*- coding: utf-8 -*-

import unittest
from processing.trigram import TrigramSet, trigram_split


class TestTrigramSet(unittest.TestCase):
    def test_init(self):
        s = TrigramSet()
        self.assertEqual(s.similar("test"), [])

    def test_add_word(self):
        s = TrigramSet()
        s.add("test")
        self.assertNotEqual(s.similar("test"), [])

    def test_add_word_multi(self):
        s = TrigramSet()
        s.add("test")
        self.assertEqual(len(s.__words__), 1)
        s.add("test")
        self.assertEqual(len(s.__words__), 1)
        s.add("TEST")
        self.assertEqual(len(s.__words__), 1)

    def test_parse_word(self):
        res = trigram_split("test")
        self.assertEqual(res, [" te", "tes", "est", "st "])

    def test_parse_word_upper_case(self):
        res = trigram_split("TEST")
        self.assertEqual(res, [" te", "tes", "est", "st "])

    def test_trigram_index_when_add_word(self):
        s = TrigramSet()
        s.add("test")
        self.assertEqual(s.__trigrams__["tes"], [0])

    def test_trigram_index_for_same_trgm(self):
        s = TrigramSet()
        s.add("testest")
        self.assertEqual(len(s.__trigrams__["est"]), 1)
        self.assertEqual(len(s.__trigrams__["tes"]), 1)

    def test_get_similar(self):
        s = TrigramSet()
        s.add("test")
        self.assertEqual(s.similar("test"), [("test", 1)])

    def test_get_similar_neq(self):
        s = TrigramSet()
        s.add("test")
        self.assertEqual(s.similar("cat"), [])

    def test_get_similar_v1(self):
        s = TrigramSet()
        s.add("test")
        self.assertEqual(s.similar("tost")[0][0], "test")

    def test_get_similar_v2(self):
        s = TrigramSet()
        s.add("test")
        self.assertEqual(s.similar("tests")[0][0], "test")

    def test_get_similar_result_size(self):
        s = TrigramSet()
        s.add("test")
        s.add("cat")
        res = s.similar("tests")
        self.assertEqual(len(res), 1)

    def test_get_similar_result_size_v2(self):
        s = TrigramSet()
        s.add("function")
        s.add("fun")
        res = s.similar("func")
        self.assertEqual(len(res), 2)
        self.assertTrue(res[0][1] > res[1][1], res)

    def test_get_similar_limit(self):
        s = TrigramSet()
        s.add("function")
        s.add("fun")
        s.add("future")
        res = s.similar("func", 0.5)
        self.assertEqual(len(res), 2)
        self.assertTrue(res[0][1] > res[1][1], res)

    def test_match(self):
        s = TrigramSet()
        s.add("function")
        s.add("fun")
        s.add("future")
        res = s.match("func")
        self.assertEqual(res[0], "fun")

    def test_match_with_limit(self):
        s = TrigramSet()
        s.add("function")
        s.add("fun")
        s.add("future")
        res = s.match("func", 0.8)
        self.assertIsNone(res)

    def test_update(self):
        s = TrigramSet()
        s.update(["function", "fun", "future"])
        self.assertEqual(len(s.__words__), 3)
