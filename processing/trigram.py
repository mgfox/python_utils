#!/usr/bin/env python
# -*- coding: utf-8 -*-


class TrigramSet(object):
    def __init__(self):
        self.__words__ = list()
        self.__trigrams__ = dict()

    def match(self, word, limit=0):
        res = self.similar(word)
        if res:
            if res[0][1] > limit:
                return res[0]
        return None

    def similar(self, word, limit=0):
        trigrams = trigram_split(word)
        words = dict()
        res = list()
        for trigram in trigrams:
            candidates = self.__trigrams__.get(trigram)
            if candidates:
                for candidate in candidates:
                    if candidate not in words:
                        words[candidate] = 1
                    else:
                        words[candidate] += 1
        for candidate in words:
            candidate_len = len(trigram_split(self.__words__[candidate]))
            if candidate_len > len(trigrams):
                k = words[candidate] / float(candidate_len)
            else:
                k = words[candidate] / float(len(trigrams))
            res.append((self.__words__[candidate], k ** 0.5))
        res.sort(lambda a, b: cmp(b[1], a[1]))
        for idx in range(len(res)):
            if res[idx][1] < limit:
                return res[:idx]
        return res

    def __sizeof__(self):
        return self.__words__.__sizeof__()

    def add(self, word):
        word = word.lower()
        if word not in self.__words__:
            self.__words__.append(word)
            idx = self.__words__.index(word)
            trigrams = trigram_split(word)
            for trigram in trigrams:
                if trigram not in self.__trigrams__:
                    self.__trigrams__[trigram] = list()
                if idx not in self.__trigrams__[trigram]:
                    self.__trigrams__[trigram].append(idx)

    def update(self, word_list):
        for word in word_list:
            self.add(word)


def trigram_split(word):
    w = " %s " % word.lower()
    res = []
    for i in range(len(w) - 2):
        res.append(w[i:i+3])
    return res
