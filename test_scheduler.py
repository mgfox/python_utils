#

import time
import logging
import datetime
from processing.scheduler import scheduled

FORMAT = '%(asctime)-15s %(message)s'
logging.basicConfig(format=FORMAT, level=logging.DEBUG)


@scheduled(every="3")
def test1():
    time.sleep(5)
    dt = datetime.datetime.now()
    print "test 1 " + dt.isoformat()


@scheduled(every="2")
def test4():
    time.sleep(7)
    dt = datetime.datetime.now()
    print "test 4 " + dt.isoformat()


@scheduled(every="1")
def test5():
    time.sleep(70)
    dt = datetime.datetime.now()
    print "test 5 " + dt.isoformat()


@scheduled(at="21:43")
def test2():
    dt = datetime.datetime.now()
    print "test 2 " + dt.isoformat()


@scheduled(every="2", days=["MON", "WED"])
def test3():
    dt = datetime.datetime.now()
    print "test 3 " + dt.isoformat()


logging.info("===")
logging.info("sleep 10 minutes....")
time.sleep(600)
logging.info("===")
