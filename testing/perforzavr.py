#!/usr/bin/env python
# coding: utf-8
__author__ = "Mike VV"

import sys
import time
import timeit
import random
import threading
import ConfigParser
try:
    import requests
except ImportError, e:
    print '"requests" library required'
    sys.exit(-1)


class TolerantConfigParser(ConfigParser.RawConfigParser):
    def __init__(self):
        ConfigParser.RawConfigParser.__init__(self, allow_no_value=True)

    def get(self, section, option):
        try:
            return ConfigParser.RawConfigParser.get(self, section, option)
        except ConfigParser.NoOptionError:
            return None


class TestConfig(object):
    def __init__(self, name, conf, default=None):
        self.name = name
        if default:
            self.base_url = conf.get(name, "base_url") or default.base_url
            self.thread_count = conf.get(name, "thread_count")\
                or default.thread_count
            self.request_count = conf.get(name, "request_count")\
                or default.request_count
            self.request_interval = conf.get(name, "request_interval")\
                or default.request_interval
            self.request_timeout = conf.get(name, "request_timeout")\
                or default.request_timeout
            self.progres_interval = conf.get(name, "progres_interval")\
                or default.progres_interval
            endpoints = conf.get(name, "endpoints")
            if endpoints:
                self.endpoints = endpoints.split(",")
            else:
                self.endpoints = default.endpoints
        else:
            self.base_url = conf.get(name, "base_url") or ""
            self.thread_count = conf.get(name, "thread_count") or 0
            self.request_count = conf.get(name, "request_count") or 0
            self.request_interval = conf.get(name, "request_interval") or 0
            self.request_timeout = conf.get(name, "request_timeout") or 0
            self.progres_interval = conf.get(name, "progres_interval") or 10
            endpoints = conf.get(name, "endpoints")
            if endpoints:
                self.endpoints = endpoints.split(",")
            else:
                self.endpoints = []

    def __str__(self):
        return "TestConfig" + str(self.__dict__)


class RequestResult(object):
    TIMEOUT = -1
    ERROR = -2
    OK = 0

    def __init__(self, status, code=0, time=0):
        self.status = status
        self.code = code
        self.time = time

    def __repr__(self):
        return self.__str__()

    def __str__(self):
        return "RequestResult(status=%s, code=%d, time=%.5f)" %\
               (self.status, self.code, self.time)


class TestStat(object):
    def __init__(self, name, results):
        self.name = name
        self.calc_counts(results)
        ok_list = self.calc_statuses(results)
        http_ok_list = filter(lambda r: r.code == 200, ok_list)
        self.calc_percentiles(http_ok_list)

    def calc_counts(self, results):
        self.total_count = len(results)
        timeout_list = filter(lambda r: r.status == RequestResult.TIMEOUT,
                              results)
        self.timeout_count = len(timeout_list)
        err_list = filter(lambda r: r.status == RequestResult.ERROR,
                          results)
        self.error_count = len(err_list)

    def calc_statuses(self, results):
        ok_list = filter(lambda r: r.status == RequestResult.OK,
                         results)
        self.statuses = {}
        for res in ok_list:
            if res.code in self.statuses:
                self.statuses[res.code] += 1
            else:
                self.statuses[res.code] = 1
        return ok_list

    def calc_percentiles(self, http_ok_list):
        self.percentiles = {}
        if not http_ok_list:
            return

        def compare(r1, r2):
            return cmp(r1.time, r2.time)
        http_ok_list.sort(cmp=compare)
        # 5% step
        for p in xrange(20):
            idx = p * len(http_ok_list) / 20
            self.percentiles[p * 5] = http_ok_list[idx].time
        self.percentiles[100] = http_ok_list[-1].time


class TestExecutor(threading.Thread):
    def __init__(self, test):
        threading.Thread.__init__(self)
        self.test = test
        self.results = []
        self.shift = random.randint(0, 1000)
        self.is_done = False
        self.request_interval = float(self.test.request_interval)

    def run(self):
        for i in xrange(int(self.test.request_count)):
            self.results.append(self.do_request(i))
            if self.request_interval:
                time.sleep(self.request_interval)
            if self.is_done:
                break
        self.is_done = True

    def do_request(self, idx):
        if self.test.endpoints:
            endpoint_idx = (self.shift + idx) % len(self.test.endpoints)
            url = self.test.base_url + self.test.endpoints[endpoint_idx]
        else:
            url = self.test.base_url
        try:
            t1 = timeit.default_timer()
            r = requests.get(url, timeout=float(self.test.request_timeout))
            t2 = timeit.default_timer()
            t = t2 - t1
            return RequestResult(RequestResult.OK, r.status_code, t)
        except requests.exceptions.Timeout, t:
            return RequestResult(RequestResult.TIMEOUT)
        except Exception, e:
            print e
            sys.exit(-1)
            return RequestResult(RequestResult.ERROR)


def show_help():
    print "try:\n\t%s [--sample] [FILENAME]" % sys.argv[0]
    print "\n\t--sample - command to generate sample conf file"
    print "\tFILENAME - name of conf file (use or generate)"


def generate_sample_conf(sample_name):
    f = open("%s.conf" % sample_name, "wb")
    f.write("""[main]
# common settings for tests
# all setting in common section may be overwritten in test

# test progress showing interval, sec (0 - disabled, default = 10)
progres_interval=10

[test_01]
# base url for test
base_url=http://localhost:8080/
# count of threads
thread_count=10
# count os requests per thread
request_count=1000
# sleep interval between requests (sec)
request_interval=5
# request timeout (sec)
request_timeout=60
# (optional) comma separated list of endpoints
# in test endpoints will be choosen by round robin
endpoints=/,/about.html,/main,/sysinfo,/

[test_02]
# ... the second test ...
""")

    f.close()


def build_test_set(params_file):
    params = TolerantConfigParser()
    params.read(params_file)
    default = TestConfig("main", params)
    test_set = []
    for test in params.sections():
        if test != "main":
            test_set.append(TestConfig(test, params, default))
    return test_set


def run_test_set(test_set):
    results = []
    for test in test_set:
        progres_interval = int(test.progres_interval)
        executor_list = []
        for idx in xrange(int(test.thread_count)):
            executor_list.append(TestExecutor(test))

        def run_test():
            test_start = time.time()
            for executor in executor_list:
                executor.start()
            while True:
                if progres_interval:
                    time.sleep(progres_interval)
                else:
                    time.sleep(2)
                is_done = True
                complete_count = 0
                for executor in executor_list:
                    if not executor.is_done:
                        is_done = False
                    complete_count += len(executor.results)
                total_req = int(test.request_count) * int(test.thread_count)
                prc_done = complete_count * 100 / total_req
                if progres_interval:
                    sys.stderr.write("test time %.3f sec, complete %.2f%%\n"
                                     % (time.time() - test_start, prc_done))
                if is_done:
                    if progres_interval:
                        sys.stderr.write("test '%s' complete\n" % test.name)
                    break
            test_result = []
            for executor in executor_list:
                test_result.extend(executor.results)
            results.append((test.name, test_result))
        try:
            run_test()
        except KeyboardInterrupt:
            for executor in executor_list:
                executor.is_done = True
            raise
    return results


def collect_stats(test_result):
    # list of tuple with:
    # - test_name
    # - list of test results
    test_stats = []
    for result in test_result:
        test_stats.append(TestStat(result[0], result[1]))
    return test_stats


def print_stats(stats):
    for stat in stats:
        print "TEST; %s" % (stat.name)
        print "REQUEST COUNT; %d" % stat.total_count
        print "ERROR COUNT;   %d" % stat.error_count
        print "TIMEOUT COUNT; %d" % stat.timeout_count
        print "STATUSES"
        k = ""
        v = ""
        for status in stat.statuses.keys():
            k += "%s; " % status
            v += "%s; " % stat.statuses[status]
        print k
        print v
        print "PERCENTILES:"
        p_list = [50, 75, 80, 90, 95, 100]
        k = ""
        v = ""
        for p in p_list:
            k += "%2d; " % p
            v += "%.3f; " % stat.percentiles[p]
        print k
        print v


def main(params):
    if len(params) == 0:
        show_help()
    elif len(params) == 2 and params[0] == "--sample":
        generate_sample_conf(params[1])
    else:
        test_set = build_test_set(params)
        test_result = run_test_set(test_set)
        stats = collect_stats(test_result)
        print_stats(stats)


if __name__ == "__main__":
    main(sys.argv[1:])
