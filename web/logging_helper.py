
import logging
import datetime
import bottle


def init_app_logging(app, log):
    def log_before_request():
        log.info(">> %s %s ..." % (bottle.request.method, bottle.request.path))
        bottle.request['__dt__'] = datetime.datetime.now()
    app.add_hook("before_request", log_before_request)

    def log_after_request():
        d = datetime.datetime.now() - bottle.request['__dt__']
        log.info("<< %s %s time: %.3f" % (bottle.request.method,
                                          bottle.request.path,
                                          d.total_seconds()))
        bottle.request['__dt__'] = datetime.datetime.now()
    app.add_hook("after_request", log_after_request)


def logged(func):
    def log_func(*p, **kw):
        s = func.__name__
        params = ""
        if p:
            params = ", ".join(p)
        if kw:
            kp = ", ".join(map(lambda x: "%s=%s" % x, kw.items()))
            if not params:
                params = kp
            else:
                params += ", %s" % kp
        logging.info("%s(%s) ...." % (s, params))
        res = func(*p, **kw)
        str_res = str(res)
        if len(str_res) > 100:
            str_res = str_res[:100] + "..."
        logging.info("%s(%s) = %s" % (s, params, str_res))
        return res
    return log_func
