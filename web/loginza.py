__author__ = 'Mike VV'

import bottle
import hashlib
from urllib2 import urlopen
import json

USER_ID = "uid"


def check_loginza_token(token, conf=None):
    loginza_uri = "http://loginza.ru/api/authinfo?token={0}"
    loginza_sec_uri = "http://loginza.ru/api/authinfo?token={0}&id={1}&sig={2}"
    secure_mode = "loginzaSecureMode"
    uri = loginza_uri.format(token)
    if conf and conf.get(secure_mode) is True:
        signature = hashlib.md5(token + conf["loginzaSecret"]).hexdigest()
        uri = loginza_sec_uri.format(token, conf["loginzaID"], signature)
    f = urlopen(uri)
    response = f.read()
    return json.loads(response)


def init_loginza_auth(app, conf, auth_func):
    USER_IDENTITY = "identity"

    @app.route('/auth', method="GET")
    def auth_redirect_page():
        bottle.redirect("/")

    @app.route('/logout')
    def logout_page():
        bottle.TEMPLATES.clear()
        bottle.request.session.clear()
        bottle.request.session.save()
        bottle.redirect("/")

    @app.route('/auth', method="POST")
    def auth_page():
        if "token" in bottle.request.params:
            auth_token = bottle.request.params["token"]
            res = check_loginza_token(auth_token, conf)
            if res:
                identity = res.get(USER_IDENTITY)
                if identity:
                    uid = auth_func(identity)
                    if uid:
                        bottle.request.session[USER_ID] = uid
                else:
                    print "loginza error: %s" % res
        bottle.request.session.save()
        return bottle.template('loginza_ok.html')


def get_login_url(site_address, provider_list=[]):
    if not provider_list:
        provider_list = ["yandex",
                         "google",
                         "mailruapi",
                         "vkontakte",
                         "facebook",
                         "twitter"]
    providers = ",".join(provider_list)
    url = "http://loginza.ru/api/widget?token_url=" + \
          "http://%s/auth&lang=ru&providers_set=%s" % (site_address, providers)
    # print "login url: " + url
    return url
