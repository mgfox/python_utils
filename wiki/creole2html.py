# coding=utf-8
__author__ = 'Mike VV'


# noinspection PyDefaultArgument
def ab_parser(search, start_tag, end_tag, exclude=[]):
    def parser(text):
        exclude_map = {}
        i = 321
        for ex in exclude:
            k = "##" + str(i) + "##"
            exclude_map[k] = ex
            text = text.replace(ex, k)
            i += 17

        o = True
        while text.find(search) > -1:
            if o:
                text = text.replace(search, start_tag, 1)
                o = False
            else:
                text = text.replace(search, end_tag, 1)
                o = True
        for k in exclude_map.keys():
            text = text.replace(k, exclude_map[k])
        return text

    return parser


def block_parser(search_start, search_end, func):
    def parser(text, **kw):
        while True:
            x = text.split(search_start, 1)
            if len(x) == 1:
                return text
            y = x[1].split(search_end, 1)
            if len(y) == 1:
                return text
            if not callable(func):
                return text
            r = func(y[0], **kw)
            text = text.replace(search_start + y[0] + search_end, r, 1)
        return text

    return parser


def tag_parser(search, tag):
    def parser(text):
        return text.replace(search, tag)

    return parser


def line_parser(line_start_tag, func):
    def parser(text):
        lines = text.split("\n")
        start_line = -1
        end_line = -1
        for idx in xrange(len(lines)):
            line = lines[idx]
            if line.strip().find(line_start_tag) == 0:
                if start_line == -1:
                    start_line = idx
                else:
                    end_line = idx
            else:
                if start_line > -1 and end_line > -1:
                    llen = len(lines[start_line:end_line + 1])
                    res = func(lines[start_line:end_line + 1])
                    if llen != len(res):
                        raise Exception("Wrong result length")
                    lines = lines[:start_line] + res + lines[end_line + 1:]
                    start_line = -1
                    end_line = -1
        if start_line > -1 and end_line > -1:
            llen = len(lines[start_line:end_line + 1])
            res = func(lines[start_line:end_line + 1])
            if llen != len(res):
                raise Exception("Wrong result length")
            lines = lines[:start_line] + res + lines[end_line + 1:]
        return "\n".join(lines)
    return parser


def processor_img(text, img_root="",
                  class_default="wiki_img",
                  class_right="wiki_img_right",
                  class_left="wiki_img_left",
                  class_center="wiki_center_container"):
    # {{Image.jpg|альтернативный текст}}
    t = text.split("|")
    img_url = t[0]
    if len(t) == 2:
        img_text = t[1]
    else:
        img_text = ""
    cls = ""
    if class_default:
        cls = " class='%s'" % class_default
    tpl = "<img src='%s' alt='%s'%s/>"
    if img_url[0] == ">":
        if class_right:
            cls = " class='%s'" % class_right
        else:
            cls = ""
        img_url = img_url[1:]
    elif img_url[0] == "<":
        if class_left:
            cls = " class='%s'" % class_left
        else:
            cls = ""
        img_url = img_url[1:]
    elif img_url[0] == "=":
        if class_center:
            cc = " class='%s'" % class_center
            tpl = "<div" + cc + "><img src='%s' alt='%s'%s/></div>"
        else:
            cls = ""
        img_url = img_url[1:]
    if "http" not in img_url:
        img_url = img_root + img_url
    return tpl % (img_url, img_text, cls)


def processor_link(text):
    # [[адрес_ссылки|текст ссылки]] <a> <audio>
    t = text.split("|")
    if len(t) == 1:
        link = text
    else:
        link = t[0]
        text = t[1]
    if link.split(".")[-1] == "mp3":
        tpl = "<audio src='%(link)s' type='audio/mpeg' controls \
style='width: 600px;'><a href='%(link)s'>%(text)s</a></audio>"
    elif link.split(":")[0] in ("http", "https"):
        tpl = "<a href='%(link)s' target='_blank'>%(text)s</a>"
    else:
        tpl = "<a href='%(link)s'>%(text)s</a>"
    return tpl % dict(link=link, text=text)


# noinspection PyAugmentAssignment
def processor_ul(lines):
    for idx in xrange(len(lines)):
        line = lines[idx].strip()
        line = "<li>" + line[1:] + "</li>"
        lines[idx] = line
    lines[0] = "<ul>\n" + lines[0]
    lines[-1] = lines[-1] + "\n</ul>"
    return lines


parse_em = ab_parser("//", "<em>", "</em>", ["http://", "https://"])
parse_strong = ab_parser("**", "<strong>", "</strong>")
parse_quote = tag_parser("''", '"')
parse_br = tag_parser("\\\\", "<br />")
parse_hr = tag_parser("----", "<hr />")
parse_img = block_parser("{{", "}}", processor_img)
parse_link = block_parser("[[", "]]", processor_link)
parse_br3 = tag_parser("\n\n\n", "\n<br /><br />\n")
parse_br2 = tag_parser("\n\n", "\n<br />\n")
parse_ul = line_parser("*", processor_ul)


def creole2html(text, img_root="", **kw):
    text = text.replace("\r\n", "\n")
    text = parse_quote(text)
    text = parse_br(text)
    text = parse_br3(text)
    text = parse_br2(text)
    text = parse_hr(text)
    text = parse_em(text)
    text = parse_strong(text)
    text = parse_img(text, img_root=img_root, **kw)
    text = parse_link(text)
    text = parse_ul(text)
    return text

