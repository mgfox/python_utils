# coding=utf-8
__author__ = 'Mike VV'

import unittest
from wiki.creole2html import *


class TestCreolToHtml(unittest.TestCase):
    def test_parse_em(self):
        self.assertEqual(parse_em("// // //"),
                         "<em> </em> <em>")
        self.assertEqual(parse_em("[[http://localhost/]]"),
                         "[[http://localhost/]]")
        self.assertEqual(parse_em("{{http://localhost/}}"),
                         "{{http://localhost/}}")

    def test_parse_strong(self):
        self.assertEqual(parse_strong("la-la **bold** la-la ****"),
                         "la-la <strong>bold</strong> la-la <strong></strong>")

    def test_parse_br(self):
        self.assertEqual(parse_br("xxx\\\\yyy\\\\zzz"),
                         "xxx<br />yyy<br />zzz")

    def test_parse_img_empty_classes(self):
        res = parse_img("la {{xx.jpg|XX}} bee", class_default="")
        exp = "la <img src='xx.jpg' alt='XX'/> bee"
        self.assertEqual(res, exp)

        res = parse_img("la {{<xx.jpg|XX}} bee", class_left="")
        exp = "la <img src='xx.jpg' alt='XX'/> bee"
        self.assertEqual(res, exp)

        res = parse_img("la {{>xx.jpg|XX}} bee", class_right="")
        exp = "la <img src='xx.jpg' alt='XX'/> bee"
        self.assertEqual(res, exp)

        res = parse_img("la {{=xx.jpg|XX}} bee", class_center="")
        exp = "la <img src='xx.jpg' alt='XX'/> bee"
        self.assertEqual(res, exp)

    def test_parse_img_default_classes(self):
        res = parse_img("la {{xx.jpg|XX}} bee")
        exp = "la <img src='xx.jpg' alt='XX' class='wiki_img'/> bee"
        self.assertEqual(res, exp)

        res = parse_img("la {{<xx.jpg|XX}} bee")
        exp = "la <img src='xx.jpg' alt='XX' class='wiki_img_left'/> bee"
        self.assertEqual(res, exp)

        res = parse_img("la {{>xx.jpg|XX}} bee")
        exp = "la <img src='xx.jpg' alt='XX' class='wiki_img_right'/> bee"
        self.assertEqual(res, exp)

        res = parse_img("la {{=xx.jpg|XX}} bee")
        exp = "la <div class='wiki_center_container'>" +\
            "<img src='xx.jpg' alt='XX' class='wiki_img'/></div> bee"
        self.assertEqual(res, exp)

    def test_parse_img_custom_classes(self):
        res = parse_img("la {{xx.jpg|XX}} bee",
                        class_default="TEST1",
                        class_right="TEST2",
                        class_left="TEST3",
                        class_center="TEST4")
        exp = "la <img src='xx.jpg' alt='XX' class='TEST1'/> bee"
        self.assertEqual(res, exp)

        res = parse_img("la {{<xx.jpg|XX}} bee",
                        class_default="TEST1",
                        class_right="TEST2",
                        class_left="TEST3",
                        class_center="TEST4")
        exp = "la <img src='xx.jpg' alt='XX' class='TEST3'/> bee"
        self.assertEqual(res, exp)

        res = parse_img("la {{>xx.jpg|XX}} bee",
                        class_default="TEST1",
                        class_right="TEST2",
                        class_left="TEST3",
                        class_center="TEST4")
        exp = "la <img src='xx.jpg' alt='XX' class='TEST2'/> bee"
        self.assertEqual(res, exp)

        res = parse_img("la {{=xx.jpg|XX}} bee",
                        class_default="TEST1",
                        class_right="TEST2",
                        class_left="TEST3",
                        class_center="TEST4")
        exp = "la <div class='TEST4'>" +\
            "<img src='xx.jpg' alt='XX' class='TEST1'/></div> bee"
        self.assertEqual(res, exp)

    def test_parse_link(self):
        self.assertEqual(parse_link("zz[[1.html]]zz"),
                         "zz<a href='1.html'>1.html</a>zz")
        self.assertEqual(parse_link("zz[[http://1/1.html|hhhh]]zz"),
                         "zz<a href='http://1/1.html' " +
                         "target='_blank'>hhhh</a>zz")
        self.assertEqual(parse_link("ss[[3.mp3]]dd"),
                         "ss<audio src='3.mp3' type='audio/mpeg' " +
                         "controls style='width: 600px;'>" +
                         "<a href='3.mp3'>3.mp3</a></audio>dd")
